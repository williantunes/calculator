//
//  ViewController.swift
//  Calculator
//
//  Created by Willian Antunes on 9/6/17.
//  Copyright © 2017 Willian Antunes. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var display: UILabel!
    
    var userIsInTheMiddleOfTyping = false
    
    @IBOutlet weak var clearButton: UIButton!
    
    @IBAction func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = display.text!
            if digit.contains(".") && textCurrentlyInDisplay.contains(".") {
            } else { display.text = textCurrentlyInDisplay + digit
            }
        } else {
            if digit.contains(".") {
                display.text = "0" + digit
            } else {
                display.text = digit
            }
            userIsInTheMiddleOfTyping = true
        }
        switchClearButton()
    }
    
    func switchClearButton() {
        if display.text == "0" {
            for state: UIControl.State in [.normal, .highlighted, .disabled, .selected, .focused, .application, .reserved] {
                clearButton.setTitle("AC", for: state)
            }
        } else {
            for state: UIControl.State in [.normal, .highlighted, .disabled, .selected, .focused, .application, .reserved] {
                clearButton.setTitle("C", for: state)
            }
        }
    }
    
    
    var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            if newValue == newValue.rounded() {
                display.text = String(newValue).components(separatedBy: ".")[0]
            } else {
            display.text = String(newValue)
            }
        }
    }
    
    private var brain = CalculatorBrain()
    
    @IBAction func performOperation(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping {
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        if let mathematicalSymbol = sender.currentTitle {
            brain.performOperation(mathematicalSymbol)
        }
        if let result = brain.result {
            displayValue = result
        }
        switchClearButton()
    }
    
}

