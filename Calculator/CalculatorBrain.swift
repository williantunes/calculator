//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Willian Antunes on 9/9/17.
//  Copyright © 2017 Willian Antunes. All rights reserved.
//

import Foundation

struct CalculatorBrain {
    
    private var accumulator: Double?
    
    private enum Operation {
        case constant(Double)
        case unaryOperation((Double) -> Double)
        case binaryOperation((Double,Double) -> Double)
        case equals
    }
    
    private var operations: Dictionary<String,Operation> = [
        "π" : Operation.constant(Double.pi),
        "e" : Operation.constant(M_E),
        "AC" : Operation.constant(0),
        "C" : Operation.constant(0),
        "√" : Operation.unaryOperation(sqrt),
        "cos" : Operation.unaryOperation(cos),
        "sin" : Operation.unaryOperation(sin),
        "±" : Operation.unaryOperation( { -$0 } ),
        "e^x" : Operation.unaryOperation( { pow(M_E, $0) } ),
        "%" : Operation.unaryOperation( { $0 / 100 } ),
        "1/x" : Operation.unaryOperation( { 1/$0 } ),
        "x" : Operation.binaryOperation( { $0 * $1 } ),
        "÷" : Operation.binaryOperation( { $0 / $1 } ),
        "+" : Operation.binaryOperation( { $0 + $1 } ),
        "-" : Operation.binaryOperation( { $0 - $1 } ),
        "＾" : Operation.binaryOperation( { pow($0, $1) } ),
        "=" : Operation.equals
    ]
    
    mutating func performOperation(_ symbol: String) {
        if let operation = operations[symbol] {
            switch operation {
            case .constant(let value):
                accumulator = value
            case .unaryOperation(let function):
                if accumulator != nil {
                    accumulator = function(accumulator!)
                }
            case .binaryOperation(let function):
                if accumulator != nil {
                    pendingBinaryOperation = PendingBinaryOperation(function: function, firstOperand: accumulator!)
                    accumulator = nil
                }
            case .equals:
                performPendingBinaryOperation()
            }
        }
    }
    
    private var pendingBinaryOperation: PendingBinaryOperation?
    
    private mutating func performPendingBinaryOperation() {
        if pendingBinaryOperation != nil && accumulator != nil {
            accumulator = pendingBinaryOperation!.perform(with: accumulator!)
            pendingBinaryOperation = nil
        }
    }
    
    private struct PendingBinaryOperation {
        let function: (Double,Double) -> Double
        let firstOperand: Double
        
        func perform(with secondOperand: Double) -> Double {
            return function(firstOperand, secondOperand)
        }
    }
    
    mutating func setOperand(_ operand: Double) {
        accumulator = operand
    }
    
    var result: Double? {
        get {
            return accumulator
        }
    }
    
}
